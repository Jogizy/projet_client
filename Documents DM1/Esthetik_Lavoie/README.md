# Projet DM1 Esthetik Lavoie

> Projet web utilisant le framework Laravel 8. 

## Table des matières
* [Info générale](#info-générale)
* [Technologies](#technologies)
* [Installation](#installation)
* [Fonctionnalités](#fonctionnalités)
* [Contact](#contact)

## Info générale
Ce projet a été réalisé dans le cadre du cours DM1 pendant la session d'automne 2020. 

Notre mandat était de faire l'analyse, la conception du projet et le code pour  pouvoir présenter à notre client un prototype fonctionnel de ce qu'il désirait.

## Technologies
* Laravel 8.12 (utilise PHP 7.4.11)

### Préalables
* Avoir PHP 7.4 ainsi qu'un serveur MySQL/MariaDB. XAMPP était la solution utilisée.
* Avoir installé Laravel 8 : https://laravel.com/docs/8.x
* Avoir installé git.

### Téléchargement du projet
Clone HTTPS à partir de gitlab en suivant le lien suivant : https://gitlab.com/Jogizy/projet_client.git

## Installation
* Créer un dossier pour mettre le projet.
* Naviguer avec l'invite de commande (cmd.exe) dans le dossier du projet.
* Entrer la commande:  
```
git clone https://gitlab.com/Jogizy/projet_client.git 
```
* Entrer dans le dossier "Esthetik_Lavoie" (tous les autres dossiers peuvent être supprimés) 
```
cd projet_client\Esthetik_Lavoie 
```
* Entrer les commandes:  
```
composer install 
```
```
npm install 
```
```
copy .env.example .env 
```
```
php artisan key:generate 
```
* Créer une base de données avec mysql (ou mariaDB) avec le nom **esthetik_lavoie_db**
* Modifier dans le fichier .env la ligne **DB_DATABASE=esthetik_lavoie_db** (et toute autre ligne pour adapter le projet a votre environnement local.)
* Faites la migration avec la commande:  
```
php artisan migrate 
```
* Remplir la base de données avec des seed:  
```
php artisan db:seed 
```

### Démarrage
Pour démmarer le projet en Laravel : 

Entrez les commandes suivantes dans un terminal (dans le dossier "Esthetik_Lavoie"):

Pour démarrer le projet (avec XAMPP déjà en marche) : 
```
php artisan serve 
```

Dans un navigateur, allez à l'adresse **localhost:8000**