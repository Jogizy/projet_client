<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paiement extends Model
{
    protected $guarded = array('id');

    public function panier()
    {
        return $this->belongsTo(Panier::class);
    }

    public function itemVendu()
    {
        return $this->hasMany(ItemVendu::class);
    }
}
