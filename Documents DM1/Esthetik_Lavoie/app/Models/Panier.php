<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Panier extends Model
{
    protected $guarded = array('id');

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function articlePanier()
    {
        return $this->hasMany(ArticlePanier::class);
    }

    public function servicePanier()
    {
        return $this->hasMany(ServicePanier::class);
    }

    public function paiement()
    {
        return $this->hasMany(Paiement::class);
    }
}
