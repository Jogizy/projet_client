<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicePanier extends Model
{
    protected $guarded = array('id');

    public function panier()
    {
        return $this->belongsTo(panier::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function vehicule()
    {
        return $this->belongsTo(Vehicule::class);
    }
}
