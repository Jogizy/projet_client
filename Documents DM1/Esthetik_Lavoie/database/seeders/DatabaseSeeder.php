<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

       // $this->call(UserSeeder::class);
       // $this->call(ClientSeeder::class);
       // $this->call(AdminSeeder::class);
        $this->call( UserSeeder::class);
        $this->call( VehiculeSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(HistoriqueVentesSeeder::class);
        $this->call(NotificationSeeder::class);
        $this->call(PanierSeeder::class);
        $this->call(ServicePanierSeeder::class);
        $this->call(ArticlePanierSeeder::class);
        $this->call(ReservationSeeder::class);
        $this->call(PaiementSeeder::class);
        $this->call(ItemVenduSeeder::class);
        $this->call(MyLiveChatSeeder::class);
        $this->call(UtilisateurNotificationSeeder::class);

    }
}
