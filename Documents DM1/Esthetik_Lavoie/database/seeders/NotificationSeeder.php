<?php

namespace Database\Seeders;

use App\Models\Notification;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('notifications')->delete();

        $n = new Notification();
        $n->type = 1;
        $n->corps= "Reduction de 2% sur service aux clients ayant effectués plus de 5 services";
        $n->save();

        $n = new Notification();
        $n->type = 1;
        $n->corps= "Rabais sur service Cirage 20%";
        $n->save();
        
        $n = new Notification();
        $n->type = 2;
        $n->corps= "Promotion sur le service lavage intérieur";
        $n->save();
    }
}
