<?php

namespace Database\Seeders;

use App\Models\Service;
use App\Models\Vehicule;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Panier;
use App\Models\ServicePanier;

class ServicePanierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_paniers')->delete();

        $pan = Panier::all();
        $sv = Vehicule::all();
        $ser = Service::all();

        $sp = new ServicePanier();
        $sp->quantite = 2 ;
        $sp->prixVehicule = 40;
        $sp->panier()->associate($pan->random());
        $sp->vehicule()->associate($sv->random());
        $sp->Service()->associate($ser->random());
        $sp->save();

        $sp = new ServicePanier();
        $sp->quantite = 3 ;
        $sp->prixVehicule = 50;
        $sp->panier()->associate($pan->random());
        $sp->vehicule()->associate($sv->random());
        $sp->Service()->associate($ser->random());
        $sp->save();

        $sp = new ServicePanier();
        $sp->quantite = 5 ;
        $sp->prixVehicule = 70;
        $sp->panier()->associate($pan->random());
        $sp->vehicule()->associate($sv->random());
        $sp->Service()->associate($ser->random());
        $sp->save();
    }
}
