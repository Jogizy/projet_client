<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $user = new User();
        $user->name = 'admin';
        $user->email = 'admin@example.com';
        $user->password = bcrypt('password');
        $user->role = 1;
        $user->adresse = '155-204 Rue paris';
        $user->telephone = '819-814-8596';
        $user->save();

        $user = new User();
        $user->name = 'client';
        $user->email = 'client@example.com';
        $user->password = bcrypt('password');
        $user->role = 2;
        $user->adresse = '155-204 Rue paris';
        $user->telephone = '819-814-8596';
        $user->save();
    }
}
