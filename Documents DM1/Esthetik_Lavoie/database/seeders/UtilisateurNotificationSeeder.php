<?php

namespace Database\Seeders;

use App\Models\Notification;
use App\Models\User;
use App\Models\UtilisateurNotification;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UtilisateurNotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('utilisateurs_notifications')->delete();

        $user1 = user::where('name', '=', 'admin')->first();
        $user2 = user::where('name', '=', 'client')->first();
        $n = Notification::all();

        $un = new UtilisateurNotification();
        $un->User()->associate($user1);
        $un->Notification()->associate($n->random());
        $un->save();

        $un = new UtilisateurNotification();
        $un->User()->associate($user2);
        $un->Notification()->associate($n->random());
        $un->save();

        $un = new UtilisateurNotification();
        $un->User()->associate($user1);
        $un->Notification()->associate($n->random());
        $un->save();

    }
}
