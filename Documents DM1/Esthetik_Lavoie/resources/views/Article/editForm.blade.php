<?php
use App\Models\Article;
if(!isset($article)) {$article= new Article;}
?>

<div class="form-group">
    {!! Form::label('nom', 'Nom:', ['class' => "col-sm-2 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('nom', $article->nom, ['class' => 'form-control']) !!}
        {{ $errors->first('nom') }}
    </div>
</div>

<div class="form-group">
    {!! Form::label('description', 'Description:', ['class' => "col-sm-2 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('description', $article->description, ['class' => 'form-control']) !!}
        {{ $errors->first('description') }}
    </div>
</div>

<div class="form-group">
    {!! Form::label('quantite_disponible', 'Quantite disponible:', ['class' => "col-sm-10 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('quantite_disponible', $article->quantite_disponible, ['class' => 'form-control']) !!}
        {{ $errors->first('quantite_disponible') }}
    </div>
</div>


<div class="form-group">
    {!! Form::label('prix', 'Prix:', ['class' => "col-sm-10 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('prix', $article->prix, ['class' => 'form-control']) !!}
        {{ $errors->first('prix') }}
    </div>
</div>

<div class="form-group">
    {!! Form::label('image', 'Image:', ['class' => "col-sm-10 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('image', $article->image, ['class' => 'form-control']) !!}
        {{ $errors->first('image') }}
    </div>
</div>






