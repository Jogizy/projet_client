@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h2>Réglages</h2>
                        </a>
                        <div class="container pt-2">
                            <h3> Liste des articles</h3>
                            {{ Form::open(['route'=> ['article.create'], 'role' => 'form', 'method' => 'get', 'class' => 'form-inline']) }}
                            <div class="form-group">
                                <div>
                                    {{ Form::submit('Créer un article', ['class' => 'btn btn-primary'])}}
                                </div>
                            </div>

                        {{ Form::close() }}
                        @if ($articles->isEmpty())
                            <p> Rien à lister.</p>
                        @else
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nom</th>
                                    <th>Description</th>
                                    <th>Quantité disponible</th>
                                    <th>Prix</th>
                                    <th>Image</th>


                                </tr>
                                </thead>
                                <tbody>
                                @foreach($articles as $article)
                                    <tr>
                                        <td><a href ="{{ route('article.show', [$article->id]) }}"> {{ $article->id }}</a> </td>
                                        <td>{{ $article->nom }}</td>
                                        <td>{{ $article->description }}</td>
                                        <td>{{ $article->quantite_disponible }}</td>
                                        <td>{{ $article->prix }}</td>
                                        <td><img src="{{ asset('Images/'.$article->image)}}" style="max-height: 60px;" alt="photo"/></td>
                                        <td><a href="{{ route('article.edit', [$article->id]) }}" class="btn btn-info">Éditer</a></td>
                                        <td>
                                            {{-- Exemple d'un popup de confirmation avant d'effacer
                                                 Le code JS est dans public/js/script.js, qui est inclue dans layout.blade.php --}}
                                            {{ Form::open(array('route' => array('article.destroy', $article->id), 'method' => 'delete', 'data-confirm' => 'Êtes-vous certain?')) }}
                                            <button type="submit" href="{{ URL::route('article.destroy', $article->id) }}" class="btn btn-danger btn-mini">Effacer</button>
                                            {{ Form::close() }}
                                        </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop
