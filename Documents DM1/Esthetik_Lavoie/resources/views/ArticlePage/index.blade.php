@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="d-flex justify-content-between">
                    <h1>Articles</h1>
                    @auth
                        @if (Auth::user()->role==1)
                            <a href="{{ route('article.create') }}" class="btn btn-primary text-justify">Ajouter un article</a>
                        @endif
                    @endauth
                </div>
                @foreach($articles as $article)
                    <div class="card mt-3">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h4 class="card-title">{{ $article->nom }}</h4>
                                <h3>{{ $article->prix }}$</h3>
                            </div>
                            <p class="card-text">{{ $article->description }}</p>
                            @auth
                                <div class="text-right">
                                    @if (Auth::user()->role == 1)
                                        <a href="{{ route('article.edit', [$article->id]) }}" class="card-link pr-3">Modifier</a>
                                    @endif
                                    @if ($article->quantite_disponible > 0)
                                        <a href="{{ route('panier.ajouterArticle', [$article->id]) }}" class="btn btn-dark">Ajouter au panier</a>
                                    @else
                                        <a href="#" class="btn btn-dark disabled">Ajouter au panier</a>
                                        <label class="d-flex justify-content-end" style="margin-bottom: -15px">En rupture de stock...</label>
                                    @endif
                                </div>
                            @endauth
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>

@stop
