@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h2>Réglages</h2>
                        </a>
                        <div class="container pt-2">
                            <h3>Création d'un service</h3>
                            {!! Form::open(['url'=> 'service', 'class' => 'form-horizontal', 'role'=>'form']) !!}
                            @include('service.editForm')
                            <div class="form-group">
                                {!! Form::submit('Créer', ['class' => 'btn btn-primary'])!!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>

        </section>
    </div>
@stop
