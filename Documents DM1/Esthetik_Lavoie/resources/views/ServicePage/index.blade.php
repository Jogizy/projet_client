@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="d-flex justify-content-between">
                    <h1>Services</h1>
                    <div class="dropdown">
                        <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Sélectionner type de Véhicule
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Automobile</a>
                            <a class="dropdown-item" href="#">Camion</a>
                            <a class="dropdown-item" href="#">VUS</a>
                        </div>
                    </div>
                    @auth
                        @if (Auth::user()->role==1)
                            <a href="{{ route('service.create') }}" class="btn btn-primary text-justify">Ajouter un service</a>
                        @endif
                    @endauth
                </div>
                @foreach($services as $service)
                    <div class="card mt-3">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h4 class="card-title">{{ $service->nom }}</h4>
                                <h3>{{ $service->prix }}$</h3>
                            </div>
                            <p class="card-text">{{ $service->description }}</p>
                            @auth
                                <div class="text-right">
                                    @if (Auth::user()->role == 1)
                                        <a href="{{ route('service.edit', [$service->id]) }}" class="card-link pr-3">Modifier</a>
                                    @endif
                                        <a href="{{ route('panier.ajouterService', [$service->id]) }}" class="btn btn-dark">Ajouter au panier</a>
                                </div>
                            @endauth
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>

@stop
