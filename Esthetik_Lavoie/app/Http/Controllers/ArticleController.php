<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreArticle;
use App\Models\Article;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return
     */
    public function index()
    {
        $articles = Article::all();
        return view('article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return
     */
    public function create()
    {
        $articles = Article::all();
        return view('article.create', compact('articles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return
     */
    public function store(StoreArticle $request)
    {
        $a= new Article();
        $a->nom = $request->nom;
        $a->description = $request->description;
        $a->quantite_disponible = $request->quantite_disponible;
        $a->prix = $request->prix;;
        $a->image = $request->image;;
        $a->save();
        return redirect('article');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return
     */
    public function show($id)
    {
        $article = Article::find($id);
        return view('article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return
     */
    public function edit($id)
    {
        $article = Article::find($id);//TODO ajouter une validation
        return view('article.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return
     */
    public function update(StoreArticle $request, $id)
    {
        $a= Article::find($id);
        $a->nom = $request->nom;
        $a->description = $request->description;
        $a->quantite_disponible = $request->quantite_disponible;
        $a->prix = $request->prix;;
        $a->image = $request->image;;
        $a->save();
        return redirect('article');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return
     */
    public function destroy($id)
    {
        $a = Article::find($id);//TODO ajouter une validation
        $a->delete();
        return redirect('article');
    }
}
