<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVehicule;
use App\Models\Vehicule;



class VehiculeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicules = Vehicule::all();//TODO ajouter une validation
        return view('vehicule.index', compact('vehicules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicules = Vehicule::all();//TODO ajouter une validation
        return view('vehicule.create', compact('vehicules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVehicule $request)
    {
        $v= new Vehicule();
        $v->type = $request->type;
        $v->prix = $request->prix;
        $v->save();
        return redirect('vehicule');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicule = Vehicule::find($id);
        return view('vehicule.show', compact('vehicule'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicule = Vehicule::find($id);
        return view('vehicule.edit', compact('vehicule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreVehicule $request, $id)
    {
        $v= Vehicule::find($id);
        $v->type = $request->type;
        $v->prix = $request->prix;
        $v->save();
        return redirect('vehicule');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $v = Vehicule::find($id);//TODO ajouter une validation
        $v->delete();
        return redirect('vehicule');
    }
}
