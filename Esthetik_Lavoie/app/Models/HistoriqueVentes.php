<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoriqueVentes extends Model
{
    protected $guarded = array('id');

    public function itemVendu()
    {
        return $this->hasMany(ItemVendu::class);
    }
}
