<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesPaniersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_paniers', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('quantite');
            $table->unsignedInteger('PrixVehicule');
            $table->unsignedBigInteger('panier_id');
            $table->unsignedBigInteger('vehicule_id');
            $table->unsignedBigInteger('service_id');
            $table->timestamps();
            $table->foreign('panier_id')->references('id')->on('paniers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('vehicule_id')->references('id')->on('vehicules')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_paniers');
    }
}
