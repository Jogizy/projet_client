<?php

namespace Database\Seeders;

use App\Models\Article;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->delete();

        $a = new Article();
        $a->nom = "pneu";
        $a->description= "permet de rouler";
        $a->quantite_disponible= 20;
        $a->prix= 20;
        $a->image= 'article1.jpg';
        $a->save();

        $a = new Article();
        $a->nom = "Protège-volant";
        $a->description= "permet la protection du volant ";
        $a->quantite_disponible= 50;
        $a->prix= 18;
        $a->image= 'article2.jpg';
        $a->save();

        $a = new Article();
        $a->nom = "Retroviseur RX";
        $a->description= " Assure une bonne visibilité ";
        $a->quantite_disponible= 42;
        $a->prix= 22;
        $a->image= 'article3.jpg';
        $a->save();
    }
}
