<?php

namespace Database\Seeders;

use App\Models\HistoriqueVentes;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HistoriqueVentesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('historique_ventes')->delete();

        $hv = new HistoriqueVentes();
        $hv->confirmation_square = "panier 45 validé";
        $hv->save();

        $hv = new HistoriqueVentes();
        $hv->confirmation_square = "panier 13 validé";
        $hv->save();

        $hv = new HistoriqueVentes();
        $hv->confirmation_square = "panier 203 validé";
        $hv->save();

    }
}
