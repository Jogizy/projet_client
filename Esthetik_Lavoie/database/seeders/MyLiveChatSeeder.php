<?php

namespace Database\Seeders;

use App\Models\MyLiveChat;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MyLiveChatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('my_live_chat')->delete();
        // $u = User::all();

        $user1 = user::where('name', '=', 'admin')->first();
        $user2 = user::where('name', '=', 'client')->first();

        $m = new MyLiveChat();
        $m->User()->associate($user1);
        $m->envoyeur= "client";
        $m->destinateur = "esthetik lavoie";
        $m->corps_message = "Salut combien coutera un service de cirage et de lavage intérieur";
        $m->save();

        $m = new MyLiveChat();
        $m->User()->associate($user2);
        $m->envoyeur= "esthetik lavoie";
        $m->destinateur = "client";
        $m->corps_message = "Le service sera de 45 pièces au total";
        $m->save();

        $m = new MyLiveChat();
        $m->User()->associate($user1);
        $m->envoyeur= "client";
        $m->destinateur = "esthetik lavoie";
        $m->corps_message = "Ok je crois que je viendrais pour ces deux services.Merci et bonne journée.";
        $m->save();
    }
}
