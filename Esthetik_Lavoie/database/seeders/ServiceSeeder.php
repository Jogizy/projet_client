<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Service;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->delete();

        $s = new Service();
        $s->nom= "Decontamination de la peinture";
        $s->description='Decontamination des surface peinte. Enlève la poussière de frein ( pico de rouille)';
        $s->image='service1.png';
        $s->prix = 50;
        $s->save();

        $s1 = new Service();
        $s1->nom="Shampoing des tapis";
        $s1->description= "Shampoing de tapis";
        $s1->image= 'photo1.png';
        $s1->prix = 55;
        $s1->save();

        $s2 = new Service();
        $s2->nom='Lavage intérieur';
        $s2->description='Lavage intérieur de base à complet';
        $s2->image='service2.png';
        $s2->prix = 60;
        $s2->save();
    }
}
