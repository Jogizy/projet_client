<?php

namespace Database\Seeders;

use App\Models\Service;
use App\Models\ServiceVehicule;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceVehiculeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_vehicules')->delete();

        $s= Service::all();

        $sv = new ServiceVehicule();
        $sv->prix = 20;
        $sv->service()->associate($s->random());
        $sv->save();

        $sv = new ServiceVehicule();
        $sv->prix = 23;
        $sv->service()->associate($s->random());
        $sv->save();

        $sv = new ServiceVehicule();
        $sv->prix = 40;
        $sv->service()->associate($s->random());
        $sv->save();

    }
}
