<?php

namespace Database\Seeders;

use App\Models\Vehicule;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehiculeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('vehicules')->delete();

        $v = new Vehicule();
        $v->type = "Auto";
        $v->prix= 15;
        $v->save();

        $v = new Vehicule();
        $v->type = "VUS";
        $v->prix= 17;
        $v->save();

        $v = new Vehicule();
        $v->type = "Camion";
        $v->prix= 19;
        $v->save();
    }
}
