@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="d-flex justify-content-between">
                    <h1>Pour nous joindre</h1>
                    @auth
                        @if (Auth::user()->role==1)
                            <a href="#" class="btn btn-primary text-justify">Modifier</a>
                        @endif
                    @endauth
                </div>
                <div class="card mt-3">
                    <div class="card-body">
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec est nibh, aliquam vel congue
                            eget, cursus nec nibh. Nullam vel ipsum eget nibh tristique egestas. Curabitur tincidunt
                            consectetur augue a aliquet. Praesent dignissim lacus justo. Mauris ultrices est vitae est
                            auctor, sed consequat orci porta. Maecenas mollis tincidunt nulla, sit amet dapibus leo
                            pharetra sed. Mauris lacinia dapibus sapien, sit amet vestibulum dui pulvinar lacinia.
                            In tristique bibendum velit, a consectetur lacus dignissim ac. Integer neque augue, sodales
                            id molestie eu, consequat id nibh. Proin semper quam id diam pellentesque suscipit. Sed
                            consectetur ex vel quam rhoncus, id gravida magna accumsan. Suspendisse in metus libero.
                            Aenean massa est, suscipit ut fringilla sit amet, venenatis viverra lorem. Integer id ornare
                            lorem, non pharetra augue. Morbi bibendum quam massa, vel vulputate turpis efficitur vel.
                            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                            Etiam magna nisi, convallis non posuere sit amet, aliquet nec justo. Phasellus convallis ex
                            eu justo vehicula, non condimentum metus tempor. Proin accumsan mattis dolor, eu vehicula
                            nisl vulputate id. Aenean nec venenatis ex, ut tincidunt nunc. Proin vulputate augue nibh,
                            sed iaculis elit maximus non. Sed massa ipsum, consectetur vel rhoncus at, mattis vel eros.
                            Suspendisse a vehicula tortor, sed ullamcorper odio. Nunc ac auctor lectus, quis commodo mi.
                            Sed fermentum purus vel tellus varius elementum quis non velit. Donec lobortis erat id
                            mattis dapibus. Curabitur at justo fringilla, tincidunt tellus ac, eleifend ante. Donec
                            tempor, sem molestie malesuada bibendum, massa nisi mollis enim, id laoreet enim turpis sit
                            amet quam. Nulla fermentum leo faucibus, convallis lacus nec, mollis arcu. Nunc commodo
                            gravida turpis. Suspendisse vitae augue sit amet nibh fermentum rhoncus. Curabitur vulputate
                            vehicula sem. Sed vel posuere sem. Suspendisse finibus, erat eget sagittis pharetra, turpis
                            lorem ullamcorper mi, id hendrerit urna ex sit amet urna. Nam convallis arcu id ornare
                            pretium. Suspendisse suscipit metus sed mauris tempor auctor. Morbi vestibulum, purus non
                            consectetur ullamcorper, velit ex lacinia arcu, vitae congue leo quam venenatis sem. Nullam
                            vitae feugiat felis. Donec nulla diam, faucibus id neque in, convallis semper metus. Nulla
                            eu semper tortor. Suspendisse potenti. Vivamus varius odio rutrum quam pretium vulputate.
                            Cras non laoreet massa, sit amet hendrerit nisl. </p>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop
