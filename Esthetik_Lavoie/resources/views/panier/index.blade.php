@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="d-flex justify-content-between">
                    <h1>Mon Panier</h1>
                </div>
                <div class="card ml-5 mr-5 mt-2 mb-2">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="col-9">Nom</th>
                            <th class="col-1">Quantité</th>
                            <th class="col-1">Prix</th>
                            <th class="col-1"> </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($itemsPanier as $item)
                            <tr>
                                <td scope="row">{{ $item->name }}</td>
                                <td class="text-center">{{ $item->quantity }}</td>
                                <td>
                                    {{ $item->price }} $
                                    {{--                                    <input type="number" value="{{ $item->quantite }}">--}}
                                </td>
                                <td>
                                    <a href="{{ route('panier.destroy', $item->id) }}">Enlever</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                    <h3 class="text-right pr-5">
                        Prix Total: {{ \Cart::session(auth()->id())->getTotal() }} $
                    </h3>

            </div>
        </section>
    </div>

@stop
