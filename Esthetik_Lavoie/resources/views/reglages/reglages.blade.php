@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Réglages</h2>
                        <div class="container pt-2">
                            <a href="{{ route('article.index') }}" style="color: #1a202c">
                                <h3>Articles</h3>
                            </a>
                            <a href="{{ route('notification.index') }}" style="color: #1a202c">
                                <h3>Notifications</h3>
                            </a>
                            <a href="{{ route('service.index') }}" style="color: #1a202c">
                                <h3>Services</h3>
                            </a>
                            <a href="{{ route('vehicule.index') }}" style="color: #1a202c">
                                <h3>Vehicules</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop
