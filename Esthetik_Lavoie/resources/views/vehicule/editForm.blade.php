<?php
use App\Models\Vehicule;
if(!isset($vehicule)) {$vehicule= new Vehicule();}
?>

<div class="form-group">
    {!! Form::label('type', 'Type:', ['class' => "col-sm-2 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('type', $vehicule->type, ['class' => 'form-control']) !!}
        {{ $errors->first('type') }}
    </div>
</div>

<div class="form-group">
    {!! Form::label('prix', 'Prix:', ['class' => "col-sm-2 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('prix', $vehicule->prix, ['class' => 'form-control']) !!}
        {{ $errors->first('prix') }}
    </div>
</div>










