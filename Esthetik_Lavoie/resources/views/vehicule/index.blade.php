@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h2>Réglages</h2>
                        </a>
                        <div class="container pt-2">
                            <h3> Liste des véhicules</h3>
                            {{ Form::open(['route'=> ['vehicule.create'], 'role' => 'form', 'method' => 'get', 'class' => 'form-inline']) }}
                            <div class="form-group">
                                <div>
                                    {{ Form::submit('Créer un vehicule', ['class' => 'btn btn-primary'])}}
                                </div>
                            </div>
                        {{ Form::close() }}
                        @if ($vehicules->isEmpty())
                            <p> Rien à lister.</p>
                        @else
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th>Prix</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($vehicules as $vehicule)
                                    <tr>
                                        <td><a href ="{{ route('vehicule.show', [$vehicule->id]) }}"> {{ $vehicule->id }}</a> </td>
                                        <td>{{ $vehicule->type }}</td>
                                        <td>{{ $vehicule->prix }}</td>
                                        <td>{{ $vehicule->annee }}</td>
                                        <td><a href="{{ route('vehicule.edit', [$vehicule->id]) }}" class="btn btn-info">Éditer</a></td>
                                        <td>
                                            {{-- Exemple d'un popup de confirmation avant d'effacer
                                                 Le code JS est dans public/js/script.js, qui est inclue dans layout.blade.php --}}
                                            {{ Form::open(array('route' => array('vehicule.destroy', $vehicule->id), 'method' => 'delete', 'data-confirm' => 'Êtes-vous certain?')) }}
                                            <button type="submit" href="{{ URL::route('vehicule.destroy', $vehicule->id) }}" class="btn btn-danger btn-mini">Effacer</button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>

@stop
