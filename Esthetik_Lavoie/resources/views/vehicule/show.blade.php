
@extends('layouts.app')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron" style="padding-top: 15px">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('reglages') }}" style="color: black">
                            <h2>Réglages</h2>
                        </a>
                        <div class="container pt-2">
                            <h3>Affichage d'un véhicule</h3>
                            @include('vehicule.editForm')
                        </div>
                    </div>
                </div>

            </div>

        </section>
    </div>
@stop
